import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import User from '../models/user';

passport.use(new LocalStrategy({
  usernameField: 'email',
},
(email, password, done) => {
  User.findOne({ email }, (err, user) => {
    if (err) { return done(err); }

    if (!user) {
      return done(null, false, { message: 'Incorrect email.' });
    }

    if (!user.isValidPassword(password)) {
      return done(null, false, { message: 'Incorrect password.' });
    }

    return done(null, user);
  });
}));

import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
opts.secretOrKey = 'shhhhhh';
passport.use(new JwtStrategy(opts, (jwtPayload, done) => {

  User.findById(jwtPayload.sub, (err, user) => {
    if (err) {
      return done(err, false);
    }

    if (!user) {
      return done(null, false);
    }

    return done(null, user);
  });
}));
