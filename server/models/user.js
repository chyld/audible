/* eslint-disable func-names */

import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
const Schema = mongoose.Schema;

const schema = new Schema({
  email: { type: String, required: true, unique: true, lowercase: true, minlength: 3 },
  password: { type: String, required: true, minLength: 3 },
  dateCreated: { type: Date, default: Date.now },
});

schema.methods.isValidPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

schema.pre('save', function (next) {
  this.password = bcrypt.hashSync(this.password, 10);
  next();
});

module.exports = mongoose.model('User', schema);
