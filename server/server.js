/* eslint-disable no-underscore-dangle */

import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import path from 'path';
import logger from './config/logging';
import favicon from 'serve-favicon';
import morgan from 'morgan';
import passport from 'passport';
import jwt from 'jwt-simple';

require('./config/database');
require('./config/authentication');

logger.log('info', '[WINSTON] - log level: %s', process.env.LEVEL);

const app = express();

app.use(compression());
app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, '../static')));
app.use(favicon(path.join(__dirname, '../static/favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());

const port = process.env.PORT || 3333;

app.listen(port, () => {
  logger.log('info', '[EXPRESS] - listening port: %d', port);
});

app.use('/things', require('./controllers/things'));

import User from './models/user';

app.post('/register', (req, res) => {
  User.create(req.body, (err, user) => {
    res.send({ err, payload: user });
  });
});

app.post('/login', passport.authenticate('local', { session: false }), (req, res) => {
  const token = jwt.encode({ sub: req.user._id, exp: (Date.now() / 1000) + 60 }, 'shhhhhh');
  res.send({ token });
});

app.get('/pokemon', passport.authenticate('jwt', { session: false }), (req, res) => {
  res.send({ yes: 'you made it', user: req.user });
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '../static/index.html'));
});

module.exports = app;
